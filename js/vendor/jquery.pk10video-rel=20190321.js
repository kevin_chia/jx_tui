function pk10video(html, main) {
    var that = this;
    this.pk10Timer = null;
    this.RankTimer = null;
    this.rootElement = $(html);
    //this.rootElement.find(".video-container").html($(html).html());
    this.E = true;
    this.F = true;
    ////
    this.timeout = -1; //计时器
    this.nowPeriod = null; //当前(将要开奖)期数
    this.lotteryTime = "";
    this.hasLottery = false;
    ////
    this.D = {
        Stime: '',
        Timer: null,
        OverRank: [],
        K: 0,
        Bg: $('.bg', that.rootElement),
        Relay: $('.relay', that.rootElement),
        Flag: true,
        TimerRadom: 0,
        Car: $('dd', that.rootElement),
        Rank: [],
        ArrRank: null
    };

   

    $("#GOTAOLAND", this.rootElement).on("click", function () {
    	gagame.Games.getCurrentCountdown().getIssueListForRefresh(function(data){
    		var gameCfg = gagame.Games.getCurrentGameConfig(); 
            var configData = gameCfg.getConfigData(); 
            configData.currentNumber = data.currentNumber;
            configData.currentNumberTime = data.currentNumberTime;
            configData.currentTime = data.currentTime;
            gagame.Games.getCurrentGame().pk10PopupObject = null;
            gagame.Games.getCurrentGame().pk10Animate(true); 
            
    	});    	
        
    });
    
    this.data = null;
};
///打开弹出框
///{prePeriod, preResult, nowPeriod, nowResult, time, lottoTime}
/*var option = {
                    lottoTime:"2017/12/30 14:51:30",
                    nowPeriod:"658935",//当前期数
                    nowResult:"",
                    prePeriod:"658934",//上一期期数
                    preResult:"2,6,5,3,8,7,1,9,10,4",//开奖结果
                    startTime:"2017/12/30 14:46:30",//开始时间
                    time:"2017/12/30 14:49:53"//当前时间
                }*/
pk10video.prototype.initData = function(options) {
    $('.pksp-periods span', this.rootElement).text('期数：' + options.prePeriod);
    if (options.nowPeriod - options.prePeriod > 1) {
        options.nowPeriod = options.prePeriod - 0 + 1;
        options.lottoTime = options.startTime;
    }
    $('#na', this.rootElement).text(options.nowPeriod);//设置下期期数
    this.getkjTime(options.lottoTime);//设置下期开奖时间
    this.getkjHistory(options.preResult);
    this.nowPeriod = options.nowPeriod;//35
    this.lotteryTime = new Date(options.lottoTime);
    options.nowPeriod = options.prePeriod;//34  
    this.refresh(options);
    
    this.data = options;
};
///刷新显示/时间矫正
///{prePeriod,preResult, nowPeriod, nowResult, time, lottoTime,nextLottoTime , nextPeriod}
pk10video.prototype.refresh = function (options) {
    var that = this;
    if (that.timeout != -1) {
        clearTimeout(that.timeout);
        that.timeout = -1;
    }
    if (that.hasLottery) return;
    //if (that.nowPeriod == options.nowPeriod) {
    if (options.nowResult.length > 0) {
        that.stop($.map(options.nowResult.split(","), function (val, idx) {
            return val - 0;
        }), options.nowPeriod, options.nextPeriod, options.nextLottoTime)
    }
    var time = (this.lotteryTime - new Date(options.time)) / 1000;
    /*var time = 5;*/
    if (time <= 0) {
        that.play("00:00");
        return;
    }
    time = Math.round(time);
    (function walk(t) {
        if (t < 1) return;
        var m = "" + Math.floor(t / 60);
        m = ("00" + m).substring(m.length);
        var s = "" + Math.floor(t % 60);
        s = ("00" + s).substring(s.length);
        that.play(m + ":" + s);
        that.timeout = setTimeout(function() {
            walk(--t);
        }, 1000);
    })(time);
};
 pk10video.prototype.reset = function() {
 	
     var that = this;
     that.gotoLand();
     this.RankTimer && clearInterval(this.RankTimer);
     this.pk10Timer && clearTimeout(this.pk10Timer);
     
     //this.stopSounds();
 };
// pk10video.prototype.stopSounds = function() {
//     this.rootElement.find("audio").each(function(idx, ele) {
//         ele.pause();
//     });
// };
///关闭弹出框
pk10video.prototype.close = function() {
    var that = this;
    if (that.timeout != -1) {
        clearTimeout(that.timeout);
        that.timeout = -1;
    }
    that.RankTimer && clearInterval(that.RankTimer);
    that.pk10Timer && clearTimeout(that.pk10Timer);
    this.rootElement.find("audio").each(function(idx, ele) {
        ele.pause();
    });
};
pk10video.prototype.play = function (stime) {
    if (this.hasLottery) return;
    var T = this,
        s = stime.split(':');
    //T.getkjTime(); //下期开奖时间
    //T.getkjHistory(); //上期开奖结果
    $('#light .clock', this.rootElement).html(stime); //倒计时时钟
    $('.track-side', this.rootElement).show(); //显示赛道边
    $('.relay', this.rootElement).removeClass('running').addClass('paused'); //赛道移动
    if (T.D.Flag) {
        if (s[0] == '00') {
            if (parseInt(s[1]) <= 30 && parseInt(s[1]) != 4 && parseInt(s[1]) != 3 && parseInt(s[1]) != 2 && parseInt(s[1]) != 1 && parseInt(s[1]) != 0) {
                T.gotoLand();
                if ($('#sound').hasClass('sound-on')) {
                    $("#ready", this.rootElement)[0].play();
                } else {
                    $("#ready", this.rootElement)[0].pause();
                };
            } else if (parseInt(s[1]) == 4) {
                if ($('#sound').hasClass('sound-on')) {
                    $("#start", this.rootElement)[0].play();
                } else {
                    $("#start", this.rootElement)[0].pause();
                };
                $('.light-red .light-raglow', this.rootElement).hide();
            } else if (parseInt(s[1]) == 3) {
                $('.light-orange .light-oaglow', this.rootElement).show();
            } else if (parseInt(s[1]) == 2) {
                $('.light-orange .light-oaglow', this.rootElement).hide();
                $('.light-green .light-gaglow', this.rootElement).show();
            } else if (parseInt(s[1]) == 1 || parseInt(s[1]) == 0) {
                $('.closeOdd', this.rootElement).show(); //封盘
                $('.relay .car-item', this.rootElement).addClass('paused');
                $('#light', this.rootElement).hide();
                $('.begin', this.rootElement).removeClass('paused').addClass('running');
                T.move();
                T.D.Flag = false;
                //if (parseInt(s[1]) == 0) {
                //    T.getRanking();
                //} else {
                    this.RankTimer = setInterval(function() {
                        T.getRanking();
                    }, 1000)
                //}
            };
        };

    };
};

//比赛停止
pk10video.prototype.stop = function(OverRank, lastAc, thisAc, nexttime) {
    var T = this,
        _kjhao = [],
        action = $('#na', this.rootElement).text() - 0;
    this.E = true;
    this.F = true;
    //T.getkjTime();
    //T.getkjHistory();
    T.CountNum(OverRank);
    this.RankTimer && clearInterval(this.RankTimer);
    this.pk10Timer && clearTimeout(this.pk10Timer);
    //期号和号码对称才会停止
    if (T.D.K && OverRank.length /*&& lastAc == action*/) {
        this.hasLottery = true;
        $('#na', this.rootElement).text(thisAc);
        $('.nextTime #nt', this.rootElement).text(nexttime);
        $('.pksp-periods span', this.rootElement).text('期数：' + lastAc);

        $('.pksp-number li').each(function (i) {
            $(this).attr('class', 'num-items')
                .addClass('carNum-' + (OverRank[i]-0))
            _kjhao.push(OverRank[i])
        });
        $('.relay', this.rootElement).css('animationPlayState', 'running').css('-webkit-animationPlayState', 'running');
        $('.side-bg', this.rootElement).addClass('paused');
        T.D.Bg.addClass('paused');
        $('.end', this.rootElement).removeClass('paused').addClass('running');
        var a = setTimeout(function() {
            $('.pksp-step1', this.rootElement).removeClass('on');
            $('.track-side', this.rootElement).hide();
            $('.pksp-step2', this.rootElement).addClass('on');
            $('.ranking-car .st', this.rootElement).attr('class', 'st car-winer-' + (OverRank[0] - 0));
            $('.ranking-car .nd', this.rootElement).attr('class', 'nd car-winer-' + (OverRank[1] - 0));
            $('.ranking-car .rd', this.rootElement).attr('class', 'rd car-winer-' + (OverRank[2] - 0));
            //document.getElementById('running').pause();
            $("#running", this.rootElement)[0].pause();
            clearTimeout(a);
        }, 2000);
        if ($('#sound').hasClass('sound-on')) {
            //document.getElementById('clapping').play();
            $("#clapping", this.rootElement)[0].play();
        } else {
            //document.getElementById('clapping').pause();
            $("#clapping", this.rootElement)[0].pause();
        }
        T.D.Flag = true;
        return;
    };
};

pk10video.prototype.move = function() {
    var T = this;
    T.D.K = 1;
    T.D.Bg.removeClass('paused');
    $('.side-bg', this.rootElement).removeClass('paused');
    this.pk10Timer = setTimeout(function() {
        T.D.Rank.length = 0;
        if ($('#sound').hasClass('sound-on')) {
            $("#ready", this.rootElement)[0].pause();
            $("#start", this.rootElement)[0].pause();
            $("#running", this.rootElement)[0].play();
        } else {
            $("#running", this.rootElement)[0].pause();
        };
        for (var i = 0; i < T.D.Car.length; i++) {
            if (!T.D.ArrRank) {
                T.D.Car[i].Distance = Math.floor(Math.random() * 601);
            }
            T.countSpeed(T.D.Car[i], T.D.Car[i].Distance, T.D.Car[i].prevDistAnce, function() {
                if (T.D.Stop == false) {
                    T.getRanking();
                }
            }.bind(T.D.Car[i]));
        }
        T.move();
    }, T.D.TimerRadom || 300)
    T.D.TimerRadom = 500 + (Math.floor(Math.random() * (6000 - 2000)));
};

pk10video.prototype.getRanking = function() {
    var cJson = [],
        T = this;
    if (this.hasLottery) return;
    $('dd', this.rootElement).each(function () {
        var cc = {};
        cc['c'] = $(this).data().num;
        cc['r'] = $(this).css('right').replace('px', '');
        cJson.push(cc);
    });
    cJson.sort(T.sortBy('r', false, parseInt));
    $('.pksp-number ul li').each(function (i) {
        $(this).attr('class', 'num-items');
        $(this).addClass('carNum-' + (cJson[i].c-0))
    });
};

//赛车移动修改html的代码
pk10video.prototype.countSpeed = function(Eml, Curreent, Prev, callBack) {
    var T = this,
        Obj = Eml,
        Bstop = true;
    clearInterval(Obj.Timers)
    Obj.Timers = setInterval(function() {
        var attrValue = T.GetStyle(Obj, 'right');
        attrValue = parseInt(attrValue.replace('px', ''));
        var Speed = (Curreent - attrValue) / 20;
        Speed = Speed > 0 ? Math.ceil(Speed) : Math.floor(Speed);
        if (attrValue != Curreent) {
            Bstop = false;
            if (attrValue > Curreent) {
                $(Obj).find('.car-speed').hide();
                $(Obj).find('.car-wheel1,.car-wheel2').removeClass('fast').addClass('slow');
            } else {
                $(Obj).find('.car-wheel1,.car-wheel2').removeClass('slow').addClass('fast');
                $(Obj).find('.car-speed').show();
            };
        } else {
            Bstop = true;
        };
        Obj.style['right'] = attrValue + Speed + "px";
        if (Bstop) {
            clearInterval(Obj.Timers);
            if (callBack) callBack();
            $(Obj).find('.car-wheel1,.car-wheel2').removeClass('fast').addClass('slow');
            $(Obj).find('.car-speed').hide();
        };
    }, 30);
};

/// 统计
pk10video.prototype.CountNum = function(data) {
    var $lh = [],
        $ds = [],
        $dx = [],
        $lu = [],
        len = 9,
        $count = {},
        $a = [],
        $b = [],
        $c = [],
        $d = [];
    var lu = [
        [3, 6, 9, 10],
        [1, 4, 7],
        [2, 5, 8]
    ];

    for (var i = 0; i < data.length; i++) {
        //龙虎
        if (i < 5) {
            if (data[i] > data[len]) {
                $lh.push('龙');
                $a.push('red');
            } else {
                $lh.push('虎');
                $a.push('blue');
            }
        };
        //单双
        if (data[i] % 2 == 0) {
            $ds.push('双');
            $b.push('red');
        } else {
            $ds.push('单');
            $b.push('blue');
        }
        //大小
        if (data[i] > 5) {
            $dx.push('大');
            $c.push('red');
        } else {
            $dx.push('小');
            $c.push('blue');
        }
        //012
        for (var j in lu) {
            if ($.inArray(parseInt(data[i]), lu[j]) != -1) {
                $lu.push(j);
                if (j == '0') {
                    $d.push('red');
                } else if (j == '1') {
                    $d.push('blue');
                } else if (j == '2') {
                    $d.push('green');
                }
            }
        };
        len = len - 1;
    };
    $count['lh'] = { 'a': $lh, 'b': $a };
    $count['ds'] = { 'a': $ds, 'b': $b };
    $count['dx'] = { 'a': $dx, 'b': $c };
    $count['lu'] = { 'a': $lu, 'b': $d };

    $('.closeOdd').hide();
    $('.tb1 table tbody tr').each(function(i) {
        $(this).find('.ball-item').each(function(j) {
            var _this = $(this);
            if (i%2 == 0) {
                var a = setTimeout(function() {
                    _this.attr('class', 'ball-item ' + $count['dx']['b'][j]).text($count['dx']['a'][j]);
                    clearTimeout(a);
                }, j * 300)

            } else {
                var a = setTimeout(function() {
                    _this.attr('class', 'ball-item ' + $count['ds']['b'][j]).text($count['ds']['a'][j]);
                    clearTimeout(a);
                }, j * 300)
            }
        })
    });
    $('.tb2 table tbody tr').each(function(i) {
        $(this).find('.ball-item').each(function(j) {
            var _this = $(this);
            if (i % 2 == 0) {
                var a = setTimeout(function() {
                    _this.attr('class', 'ball-item ' + $count['lh']['b'][j]).text($count['lh']['a'][j]);
                    clearTimeout(a);
                }, j * 300)

            } else {
                var a = setTimeout(function() {
                    _this.attr('class', 'ball-item ' + $count['lu']['b'][j]).text($count['lu']['a'][j]);
                    clearTimeout(a);
                }, j * 300)
            }
        })
    })
};

pk10video.prototype.setSound = function(el) {
    if (el.hasClass('sound-on')) {
        el.removeClass('sound-on').addClass('sound-off').attr('title', '点击开启声音');
       
    } else {
        el.removeClass('sound-off').addClass('sound-on').attr('title', '点击开启声音');
       
    }
};

pk10video.prototype.gotoLand = function() {
    this.pk10Timer = null;
    this.RankTimer = null;
    $('.relay', this.rootElement).removeAttr('style');
    $('.pksp-step2', this.rootElement).removeClass('on');
    $('.pksp-step1', this.rootElement).addClass('on');
    $('#light', this.rootElement).show();
    $('.end', this.rootElement).removeClass('running').addClass('paused');
    $('.begin', this.rootElement).removeClass('running').addClass('paused');

    $('.light-green .light-gaglow', this.rootElement).hide();
    $('.light-red .light-raglow', this.rootElement).show();
    $('.track-side', this.rootElement).show();
    var $r = 22;
    $('.relay dd', this.rootElement).each(function () {
        $(this).attr('style', 'right:' + $r + 'px');
        $(this).find('.car-wheel1,.car-wheel2').removeClass('slow');
        $r++;
    });
};

//下期开奖时间
pk10video.prototype.getkjTime = function(time) {
    if (this.E) {
        $('.nextTime #nt', this.rootElement).text(time);
        this.E = false;
    };
};

//取上期开奖号码
pk10video.prototype.getkjHistory = function (kjhao) {
    if (!kjhao) return;
    var T = this;
    if (this.F) {
        var kjhao = kjhao.split(',')
        kjhao = $.map(kjhao, function (val) {
            return val - 0;
        });
        $('.pksp-number li').each(function (i) {
            $(this).attr('class', 'num-items')
                .addClass('carNum-' + kjhao[i])
        })
        T.CountNum(kjhao);
        this.F = false;
    }
};

pk10video.prototype.GetStyle = function(Obj, name) {
    return Obj.currentStyle ? Obj.currentStyle[name] : getComputedStyle(Obj, false)[name];
};

pk10video.prototype.sortBy = function(filed, rev, primer) {
    rev = (rev) ? -1 : 1;
    return function(a, b) {
        a = a[filed];
        b = b[filed];
        if (typeof(primer) != 'undefined') {
            a = primer(a);
            b = primer(b);
        }
        if (a < b) { return rev * 1; }
        if (a > b) { return rev * -1; }
        return 1;
    }
};