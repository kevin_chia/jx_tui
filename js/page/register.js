(function($) {
	
//	var isLinkCreate = true;
	var linkUrl = route.$.linkCreateUser + "&terminal_id="+terminal_id;
	
	var configObj = {
		write_email: {
			text: '邮箱',
			reg: /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/g
		},
		write_mobile: {
			text: '手机号',
			reg: /^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$/g
		},
		write_qq: {
			text: 'QQ号',
			reg: /[1-9][0-9]{4,}/g
		},
		write_skype: {
			text: 'Skype账号',
			reg: null
		},
		write_wechat: {
			text: '微信号',
			reg: null
		}
	}
	var prize = getQueryString('prize');
	var linkInofObj = null;

	if(prize) {
		ajaxData({
			url: route.$.GetLinkInfo,
			data: {
				terminal_id: terminal_id,
				prize: prize
			},
			successCallback: function(data) {
				linkInofObj = data;
				//usernameFLAG
				var tmpl = $('#usernameTmpl').html(),
					doTtmpl = doT.template(tmpl);
				var content = doTtmpl(data);
				$('.usernameFLAG').after(content);

			}
		});
	}
	var a = getQueryString('a');
	$('#inviteCode').val(a);
	

	var inputTip = new gagame.Tip({
		cls: 'j-ui-tip-b w-4'
	});

	function showTips($t) {
		if(!$t || !$t.length) return false;
		var $tips = $t.parents('li:eq(0)').find('.feild-static-tip');
		if($tips.length) {
			var text = $tips.text();
			inputTip.setText(text);
			inputTip.show(5, inputTip.getDom().height() * -1 - 22, $t);
		}
	}

	$('.input').on('focus', function() {
		showTips($(this));
		//$(this).siblings('.ui-text-prompt').hide();
	}).on('blur', function() {
		inputTip.hide();
	});

	var name = $('#J-name'),
		username = $('#J-username'),
		password = $('#J-password'),
		// passwordHidden = $('#J-password-hidden'),
		password2 = $('#J-password2'),
		email = $('#J-email'),
		vcode = $('#J-vcode'),
		showPass = $('.J-checkbox-showpas'),
		errors = $('.ui-text-prompt');

	username.blur(function() {
		var dom = username,
			v = $.trim(dom.val()),
			tip = dom.siblings('.ui-text-prompt'),
			right = dom.siblings('.ico-right');
		if(!(/^[a-zA-Z][a-zA-Z0-9]{5,15}$/).test(v)) {
			showTips(username);
			return;
		} else {
			tip.hide();
			right.show();
		}
	});

	password.blur(function() {
		var dom = password,
			v = $.trim(dom.val()),
			tip = dom.siblings('.ui-text-prompt'),
			right = dom.siblings('.ico-right'),
			v2;
		if(!v) {
			tip.html('密码不能为空，请输入密码').show();
			right.hide();
			return;
		}
		if(!(/^(?=.*\d+)(?=.*[a-zA-Z]+)(?!.*?([a-zA-Z0-9]+?)\1\1).{6,16}$/).test(v)) {

			showTips(password);
			return;
		}

		v2 = $.trim(password2.val());
		if(v2 != '') {
			if(v != v2) {
				password2.siblings('.ui-text-prompt').show();
				password2.siblings('.ico-right').hide();
			} else {
				password2.siblings('.ui-text-prompt').hide();
				password2.siblings('.ico-right').show();
			}
		}
	});
	password2.blur(function() {
		var dom = password2,
			v = $.trim(dom.val());
		if(v != $.trim(password.val())) {
			showTips(password2);
			return;
		}
		if(v != '') {

		}
	});

	// 显示隐藏密码
	showPass.on('click', function() {
		if($(this).hasClass('active')) {
			password.prop('type', 'password');
			// passwordHidden.hide();
			$(this).attr('title', '显示密码');
		} else {
			password.prop('type', 'text');
			// passwordHidden.show();J-nickname
			$(this).attr('title', '隐藏密码');
		}
		$(this).toggleClass('active');
		return false;
	});

	function changeCaptcha() {
		/*生成验证码*/
		ajaxData({
			url: route.$.getCaptcha,
			data: {
				terminal_id: terminal_id
			},
			successCallback: function(res) {
				$("#captchaImg2").attr("src", res.src);
			}
		})
	}

	var isClickStatus = true;
	$('#J-button-submit').click(function() {
		if(!isClickStatus) return;

		if($('#J-form-panel').find('.ico-error:visible').size() > 0) {
			return false;
		}

		if(username.val() == '') {
			layer.alert('用户名不能为空');
			return false;
		}
		if($.trim(username.val()).length < 6 || $.trim(username.val()).length > 16) {
			username.focus();
			return false;
		}

		if(linkInofObj) {
			for(var o in linkInofObj) {
				if(linkInofObj[o] == 1) {
					if($('#' + o).val() == '') {
						layer.alert(configObj[o].text + '不能为空');
						return false;
					}
					if(configObj[o].reg != null) {
						if(!configObj[o].reg.test($('#' + o).val())) {
							layer.alert(configObj[o].text + '验证不通过');
							return false;
						}
					}
				}
			}
		}

		if(password.val() == '') {
			layer.alert('用户密码不能为空');
			password.focus();
			return false;
		}

		if($.trim(password.val()).length < 6 || $.trim(password.val()).length > 16) {
			password.focus();
			return false;
		}

		if(password2.val() == '') {
			layer.alert('确认密码不能为空');
			return false;
		}

		if($.trim(password2.val()).length < 6 || $.trim(password2.val()).length > 16) {
			password2.focus();
			return false;
		}

		if(password.val() != password2.val()) {
			layer.alert('确认密码与用户密码不一致');
			return false;
		}
		
		/*if(!isLinkCreate && $.trim($('#inviteCode').val()).length==0){
			layer.alert('请输入邀请码');
			return false;
		}*/

		if(vcode.val() == '') {
			layer.alert('验证码不能为空');
			return false;
		}

		if(!(/^[a-zA-Z0-9]{5}$/).test(vcode.val())) {
			vcode.focus();
			return;
		}
		isClickStatus = false;

		var formData = parseQueryString(decodeURIComponent($('#signupForm').serialize()));
		formData.prize = getQueryString("prize");
		formData.username = formData.username.toLocaleLowerCase();
		formData.nickname = formData.username;

		for(var o in linkInofObj) {
			if(linkInofObj[o] == 1) {
				formData[o] = $('#' + o).val();
			}
		}

		ajaxData({
			url: linkUrl,
			type: "post",
			data: formData,
			successCallback: function(res) {
				layer.alert('注册成功!', {
					icon: 1,
					closeBtn: 0
				}, function() {
					window.location.href = "/";
				});
				isClickStatus = true;
			},
			errorCallback: function(msg) {
				layer.alert(msg.error);
				changeCaptcha();
				isClickStatus = true;
			}

		})

		return false;

	});
	
	
	function init(){
		ajaxData({
			url: route.$.getBasicInfo + "&terminal_id=1",
			successCallback: function(data) {
				if(data.invite_code_enabled == 1){
//					linkUrl = route.$.InviteCodeCreateUser + "&terminal_id="+terminal_id;
					$('#inviteCodeBox').show();
//					isLinkCreate = false;
				}else{
					$('#inviteCodeBox').hide();
//					linkUrl = route.$.linkCreateUser + "&terminal_id="+terminal_id;
//					isLinkCreate = true;
				}
				linkUrl = route.$.InviteCodeCreateUser + "&terminal_id="+terminal_id;
			}
		});
	}
	
	init();
	
	changeCaptcha();
	name.click().focus();
	window.changeCaptcha = changeCaptcha;
	window.configObj = configObj;
})(jQuery);