var route = route || {};

(function() {
	var isDebug = false;
	
	var that = this; 
	that.originDomain = window.location.origin;
	that.websiteAddress = (localStorage.getItem('websiteAddress') || that.originDomain);
	//that.websiteAddress = window.location.origin+"/";
	
	that.$$ = that.websiteAddress + "/service";
	if(isDebug){
		that.$$ = 'https://jx08.net/service';
	}

	that.setDomain = function(domain) {
		that.$$ = domain + "service";
	}

	that.setDollar = function() {
		that.$ = that.getDollar();
	} 
	

	that.getDollar = function() {

		var routes = {
			//登录
			login: that.$$ + '?action=Login',
			//获取用户信息
			userInfo: that.$$ + '?action=getCurrentUserInfo',
			//修改登录密码
			changeLoginPwd: that.$$ + "?action=changeLoginPwd",
			//修改资金密码
			changeFundPwd: that.$$ + "?action=changeFundPwd",
			//设置资金密码
			setFundPwd: that.$$ + "?action=setFundPwd",
			//获取游戏记录列表
			getProjectList: that.$$ + "?action=getProjectList",
			//登出
			logout: that.$$ + "?action=logout",
			//获取银行卡列表
			getBankCardList: that.$$ + "?action=getBankCardList",
			//获取公告列表
			getNoticeList: that.$$ + "?action=getNoticeList",
			//公告详情
			getNoticeDetail: that.$$ + "?action=getNoticeDetail",
			//站内信
			getUserLetters: that.$$ + "?action=getUserLetters",
			//获取发送站内信的信息
			getSendInfo: that.$$ + "?action=getSendInfo",
			//发送站内信
			sendLetter: that.$$ + "?action=sendLetter",
			//阅读站内信
			readLetter: that.$$ + "?action=readLetter",
			//追号记录
			getTraceList: that.$$ + "?action=getTraceList",
			//追号清单
			getTraceProjectDetail: that.$$ + "?action=getTraceProjectDetail",
			//取消单期追号
			CancelTraceReserve: that.$$ + "?action=CancelTraceReserve",
			//撤单
			dropProject: that.$$ + "?action=dropProject",
			//注单详情
			getProjectDetail: that.$$ + "?action=getProjectDetail",
			//追号详情
			getTraceDetail: that.$$ + "?action=getTraceDetail",
			
			getTraceProjectDetail: that.$$ + "?action=getTraceProjectDetail",
			
			
			//终止追号
			stopTrace: that.$$ + "?action=stopTrace",
			//资金明细-即账变记录
			getTransactionList: that.$$ + "?action=getTransactionList",
			//团队盈亏   团队报表
			getTeamGameTypeProfits: that.$$ + "?action=getTeamGameTypeProfits",
			//用户盈亏
			getUserGameTypeProfits: that.$$ + "?action=getUserGameTypeProfits",

			//验证银行卡
			checkBankCard: that.$$ + "?action=checkBankCard",
			//获取银行卡列表
			getBankList: that.$$ + "?action=getBankList",
			//绑定银行卡
			bindBankCard: that.$$ + "?action=bindBankCard",
			//修改银行卡
			changeBankCard: that.$$ + "?action=changeBankCard",
			//删除银行卡
			deleteBankCard: that.$$ + "?action=deleteBankCard",
			//锁定银行卡
			lockBankCard: that.$$ + "?action=lockBankCard",
			//getGameMenu
			getGameMenu: that.$$ + "?action=GetGameMenu",
			//获取投注数据
			getGameSettingForBet: that.$$ + "?action=getGameSettingForBet",
			//获取账户数据
			getAccount: that.$$ + "?action=getAccount",
			////获取奖金组相关数据
			getPrizeGroupForCreate: that.$$ + "?action=getPrizeGroupForCreate",
			//获取开奖奖期
			getIssueListForRefresh: that.$$ + "?action=getIssueListForRefresh",
			//
			withdraw: that.$$ + "?action=withdraw",
			//获取走势图相关数据
			GetTrendData: that.$$ + "?action=GetTrendData",
			//用户列表
			getUsers: that.$$ + "?action=getUsers",
			//获取开奖信息
			GetWnNumbersOfAllGames: that.$$ + "?action=GetWnNumbersOfAllGames",
			//获取某个彩种开奖信息
			GetWnNumberList: that.$$ + "?action=GetWnNumberList",
			//开户
			MobileCreateUser: that.$$ + "?action=MobileCreateUser",
			getPrizeList: that.$$ + "?action=getPrizeList",
			//设置昵称
			SetNickname: that.$$ + "?action=SetNickname",
			//获取充值渠道
			getDepositPlatformList: that.$$ + "?action=getDepositPlatformList",
			
			getAvailablePaymentType: that.$$ + "?action=getAvailablePaymentType",

			deposit: that.$$,
			//注册
			linkCreateUser: that.$$ + "?action=linkCreateUser",
			//获取验证码
			getCaptcha: that.$$ + "?action=getCaptcha",
			//获取验证码状态
			GetLoginCaptchaStatus: that.$$ + "?action=GetLoginCaptchaStatus",
			//删除邮件
			deleteLetter: that.$$ + "?action=deleteLetter",
			//分红处理
			getShares: that.$$ + "?action=getShares",

			//获取下级用户列表
			getChildren: that.$$ + "?action=getChildren",
			//获取下级用户列表
			getTeamLotteryProfits: that.$$ + "?action=getTeamLotteryProfits",

			//契约下级
			getSubUserInfo: that.$$ + "?action=getSubUserInfo",

			//契约分红
			getUserDividendReport: that.$$ + "?action=getUserDividendReport",

			//日工资信息
			getUserSalaryReport: that.$$ + "?action=getUserSalaryReport",
			//契约签订
			setChildDividendContract: that.$$ + "?action=setChildDividendContract",
			//我的契约工资
			getUserSalaryContract: that.$$ + "?action=getUserSalaryContract",
			//我的分红 - 我的契约
			getUserDividendContract: that.$$ + "?action=getUserDividendContract",

			//密保问题
			getSecurityQuestions: that.$$ + "?action=getSecurityQuestions",
			//密保设置
			setSecurityAnswer: that.$$ + "?action=setSecurityAnswer",

			//获取奖金组设置信息
			getPrizeGroupForSet: that.$$ + "?action=getPrizeGroupForSet",

			//设置奖金组
			setUserPrizeGroup: that.$$ + "?action=setUserPrizeGroup",

			//绑定取款人
			setRealname: that.$$ + "?action=setRealname",

			//获取转账类型列表
			getTransactionTypeList: that.$$ + "?action=getTransactionTypeList",

			//转账
			doTransfer: that.$$ + "?action=doTransfer",

			//同意契约
			agreeDividendContract: that.$$ + "?action=agreeDividendContract",

			//拒绝契约
			rejectDividendContract: that.$$ + "?action=rejectDividendContract",

			//发送分红
			sendDividendReport: that.$$ + "?action=sendDividendReport",
			//拒绝契约
			createRegisterLink: that.$$ + "?action=createRegisterLink",

			//链接管理列表
			getRegisterLinks: that.$$ + "?action=getRegisterLinks",
			
			//
			deleteRegisterLink: that.$$ + "?action=deleteRegisterLink",
			

			//转账管理 开关
			subTransferPower: that.$$ + "?action=subTransferPower",

			//获取配额列表
			getUserQuotas: that.$$ + "?action=getUserQuotas",

			//设置配额
			setUserQuotas: that.$$ + "?action=setUserQuotas",

			//提现规则
			getWithdrawalRules: that.$$ + "?action=getWithdrawalRules",
			//提现记录
			getWithdrawalList: that.$$ + '?action=getWithdrawalList',
			//充值记录
			getDepositList: that.$$ + '?action=getDepositList',

			//获取银行卡充值记录
			getBankDepositList: that.$$ + '?action=getBankDepositList',

			//同意日工资契约
			agreeSalaryContract: that.$$ + '?action=agreeSalaryContract',

			//拒绝日工资契约
			rejectSalaryContract: that.$$ + '?action=rejectSalaryContract',

			//契定日工资契约
			setChildSalaryContract: that.$$ + '?action=setChildSalaryContract',
			//获取用户余额
			getUserMoney: that.$$ + '?action=getUserMoney',

			//app升级
			getLatestRelease: that.$$ + '?action=getLatestRelease',

			//获取客服链接
			getClientServices: that.$$ + '?action=getClientServices',

			//获取线路
			getDomain: that.$$ + '?action=getDomain',
			
			getUnreadNum: that.$$ + '?action=getUnreadNum',
			
			//游戏类型
			getGameTypeList: that.$$ + '?action=getGameTypeList',
			
			
			//彩种
			getLotteryList: that.$$ + '?action=getLotteryList',
			
			//支付方式
			getPaymentType: that.$$ + '?action=getPaymentType',
			
			//充值渠道
			getPlatformList: that.$$ + '?action=getPlatformList',
			
			//彩票状态 
			getProjectStatus: that.$$ + '?action=getProjectStatus',
			
			//彩系
			getGameSeries: that.$$ + '?action=getGameSeries',
			
			//追号状态
			getTraceStatus: that.$$ + '?action=getTraceStatus',
			
			//用户报表
			getUserGameTypeProfitsForParent: that.$$ + '?action=getUserGameTypeProfitsForParent',
			
			//人工开户
			accurateCreateUser: that.$$ + '?action=accurateCreateUser',
			
			//帮助中心
			helpCenter: that.$$ + '?action=helpCenter',
			
			//挽回密码
			findPwd: that.$$ + '?action=findPwd',
			
			//基础配置信息
			getBasicInfo: that.$$ + '?action=getBasicInfo',
			
			//昨日派奖金额
			getPlatProfit: that.$$ + '?action=getPlatProfit',

			//获取活动列表
			GetAllEvents: that.$$ + '?action=GetAllEvents',
			
			getAvailablePlats: that.$$ + '?action=getAvailablePlats',
			
			getPlatAccount: that.$$ + '?action=getPlatAccount',
			
			printProjects: that.$$ + '?action=printProjects',
			
			platTransfer: that.$$ + '?action=platTransfer',
			
			//免费试玩
			TryPlayUserLogin: that.$$ + '?action=TryPlayUserLogin',
			
			GetLinkInfo: that.$$ + '?action=GetLinkInfo',
			
			//vip签到
			TakeVipSign: that.$$ + '?action=TakeVipSign',
			//获取用户vip等级接口
			GetUserGrade: that.$$ + '?action=GetUserGrade',
			//获取用户任务进度接口
			GetUserVipTaskSchedule: that.$$ + '?action=GetUserVipTaskSchedule',
			//获取用户vip升级奖励 接口
			GetUserVipGradeBonus: that.$$ + '?action=GetUserVipGradeBonus',
			
			//奖励明细
			GetUserVipTask: that.$$ + '?action=GetUserVipTask',
			
			//获取任务礼包详情
			GetVipTaskDetail: that.$$ + '?action=GetVipTaskDetail',

			//长龙
			GetChangLongData:that.$$ + '?action=GetChangLongNewData',

			//历史开奖活动详情
			GetGameHIstoryData:that.$$ + '?action=GetGameHIstoryData',
			
			InviteCodeCreateUser: that.$$ + '?action=InviteCodeCreateUser',
			
			//获取奖金组列表
			getPrizeGroups: that.$$ + '?action=getPrizeGroups',
			
			//动态获取banner
			GetBannerList: that.$$ + '?action=GetBannerList',
			
			//第三方游戏数据获取接口
			GetThirdLotteryLink: that.$$ + '?action=GetThirdLotteryLink',
			
			//频道转账
			DoThirdTransfer: that.$$ + '?action=DoThirdTransfer',
			
			//第三方投注记录
			getThirdProjectList: that.$$ + '?action=getThirdProjectList',
			
			//获取取到金额
			GetThirdLotteryBalance: that.$$ + '?action=GetThirdLotteryBalance',
			
			//第三方团队盈亏
			GetThirdPlatTeamProfits: that.$$ + '?action=GetThirdPlatTeamProfits',

			//第三方用户盈亏
			getThirdPlatUserProfits: that.$$ + "?action=getThirdPlatUserProfits",
			//我的契约工资,用这个，上面那个会获取不到不知道为啥
			getUserSalary1: that.$$ + "?action=getUserSalaryContract",
			//契定日工资契约,用这个，上面那个会获取不到不知道为啥
			setChildSalary1: that.$$ + '?action=setChildSalaryContract',
			
			


		};
		  
		
		return routes;
		
	}

	//foo
	that.$ = that.getDollar();

}).apply(route);