;
(function(w, $) {
	w.host = window.location.origin;
	w.forward = window.location.href;
	var terminal_id = 1;
	
	w.loginAlertTip = 1; //未登录弹窗提示，避免多次弹窗

	var getWebsiteOfHost = function() {
		return window.location.origin +  "/service";
	};

	//公共处理事件
	initCommonEvents();

	function initCommonEvents() {
		//金额输入格式话
		$(document).on("keyup", "[formatmoney]", function(e) {
			var v = $.trim(this.value),
				arr = [],
				code = e.keyCode;
			if(code == 37 || code == 39) {
				return;
			}
			v = v.replace(/[^\d|^\.]/g, '');
			arr = v.split('.');
			if(arr.length > 2) {
				v = '' + arr[0] + '.' + arr[1];
			}
			arr = v.split('.');
			if(arr.length > 1) {
				arr[1] = arr[1].substring(0, 2);
				v = arr.join('.');
			}
			this.value = v;
			v = v == '' ? '&nbsp;' : v;
		}).on("blur", "[formatmoney]", function(e) {
			var v = Number(this.value),
				maxNum = Number($(this).attr('max-data'));
			if(maxNum > 0) {
				v = v > maxNum ? maxNum : v;
			}
			this.value = v;
		});

	}

	/*ajax封装*/
	function ajaxData(obj) {
		var type = obj["type"] || "GET";
		var async = true;
		var data = obj["data"] || {};
		if(typeof obj["async"] !== "undefined") {
			async = obj["async"];
		}
		var url = obj["url"];
		
		var token = window.localStorage.getItem("__token__");
		data.terminal_id = terminal_id;
		data.token = token; 
		
		$.ajax({
			url: url,
			type: type,
			data: data,
			async: async,
			beforeSend: function() {
				if(typeof obj.before === "function") {
					obj.before();
				}
			},
			success: function(msg) {
				if(typeof msg === 'string') {
					msg = JSON.parse(msg);
				}
				//console.log(msg);
				if(msg.errno == 0) {
					if(typeof obj.successCallback === "function") {
						obj.successCallback(msg.data);
					}
				} else if(msg.errno == 3004) { 
					w.localStorage.setItem("loginFlag", false);
					w.localStorage.setItem("terminal_id", "");

					$(".game-load").hide(); //游戏页面需要隐藏loading，预防遮住提示框
					
					
					$('.loading-mask').hide();	
					
					loginAlertTip = top.loginAlertTip;
					if(loginAlertTip == 1) {
						loginAlertTip = 0;
						$('.loading-mask').hide(); 
						layer.confirm('登录过期，请重新登录', {
							btn: ['确定'], //按钮
							title: '提示',
							closeBtn: 0,
							title: '温馨提示'
						}, function() { //确定回调函数
							if(top){
								top.window.location.href = "/index.shtml";
							}else{
								w.location.href = "/index.shtml";	
							}
							
						});
					}

				}else if(msg.errno == 1101){
					//access_ip
					if(top){
						top.window.location.href = "/view/page/noService.shtml?access_ip="+data.access_ip;
					}else{
						w.location.href = "/view/page/noService.shtml?access_ip="+data.access_ip;
					}
					
				}else {
					if(typeof obj.errorCallback === "function") {
						obj.errorCallback(msg);
					} else {
						layer.alert(msg.error);
					}
				}
			},
			complete:function(){
				$('.loading-mask').hide();
			},
			error:function(){
				console.log("请求超时");
			}

		})
	}

	/*
	options = {
	    tmplObj:  selector id
	    data: data
	    needClear: boolean
	    prependDom : boolean
	}
	*/
	function doTrender(options) {
		var tmpl = $(options.tmplObj).html(),
			doTtmpl = doT.template(tmpl);
		var content = doTtmpl(options.data);
		if(options.needClear) {
			$(options.container).html('');
		}
		if(options.prependDom) {
			$(options.prependDom).before(content);
		} else {
			$(options.container).append(content);
		}
	}

	//通过彩系id获取对应模板
	function getLotteryUrlBySeriesId(prefix) { 
		return {
		'2': {
			url:prefix + '/11x5.shtml?',
			num_type:5
		},
		'4': {
			url:prefix + '/k3.shtml?',
			num_type:3
		},
		'8': {
			url:prefix + '/kl12.shtml?',
			num_type:5
		},
		'9': {
			url:prefix + '/klsf.shtml?',
			num_type:8
		},
		'1': {
			url:prefix + '/ssc.shtml?',
			num_type:5
		},
		'3': {
			url:prefix + '/3d.shtml?',
			num_type:3
		},
		'7': {
			url:prefix + '/kl8.shtml?',
			num_type:20
		},
		'5': {
			url:prefix + '/pk10.shtml?',
			num_type:10
		},
		'10': {
			url:prefix + '/liuhecai.shtml?',
			num_type:7
		}
	}
	}
	
	

	function getLotteryNavObj(prefix, imagePath) {
		var game = {
			'11x5': {
				url: prefix + '/11x5.shtml?',
				image: imagePath + '/SH11X5.png',
				num_type: 5
			},
			k3: {
				url: prefix + '/k3.shtml?',
				image: imagePath + '/JSKS.png',
				num_type: 3
			},
			kl12: {
				url: prefix + '/kl12.shtml?',
				image: imagePath + '/kl12.png',
				num_type: 5
			},
			klsf: {
				url: prefix + '/klsf.shtml?',
				image: imagePath + '/GXKS.png',
				num_type: 8
			},
			pk10: {
				url: prefix + '/pk10.shtml?',
				image: imagePath + '/BJPKS.png',
				num_type: 10
			},
			ssc: {
				url: prefix + '/ssc.shtml?',
				image: imagePath + '/ZQSSC.png',
				num_type: 5
			},
			kl8: {
				url: prefix + '/kl8.shtml?',
				image: imagePath + '/kl8.png',
				num_type: 20
			},
			d3: {
				url: prefix + '/3d.shtml?',
				image: imagePath + '/3d.png',
				num_type: 3
			},
			pl: {
				url: prefix + '/ssc.shtml?',
				image: imagePath + '/pl3.png',
				num_type: 5
			},
			sport: {
				url: '',
				image: ''
			},
			mmc: {
				url: prefix + '/mmc.shtml?',
				image: ''
			},
			pg115mmc: {
				url: prefix + '/115mmc.shtml?',
				image: ''
			},
			liuhecai: {
				url: prefix + '/liuhecai.shtml?',
				image: ''
			}
		}

		return {
			'2': game['11x5'],
			'8': game['11x5'],
			'9': game['11x5'],
			'14': game['11x5'],
			'22': game['11x5'],
			'23': game['11x5'],
			'24': game['11x5'],
			'25': game['11x5'],
			'27': game['11x5'],
			'29': game['11x5'],
			'32': game['11x5'],
			'34': game['11x5'],
			'43': game['pg115mmc'],
			'44': game['11x5'],
			'47': game['11x5'],
			'15': game['k3'],
			'17': game['k3'],
			'18': game['k3'],
			'21': game['k3'],
			'30': game['k3'],
			'33': game['k3'],
			'48': game['k3'],
			'39': game['kl12'],
			'40': game['klsf'],
			'41': game['klsf'],
			'42': game['kl12'],
			'50': game['klsf'],

			'10': game['pk10'],
			'19': game['pk10'],
			'116': game['pk10'],

			'31': game['sport'],

			'1': game['ssc'],
			'3': game['ssc'],
			'4': game['d3'],
			'6': game['ssc'],
			'7': game['ssc'],
			'13': game['ssc'],
			'16': game['ssc'],
			'20': game['d3'],
			'26': game['mmc'],
			'28': game['ssc'],
			'45': game['ssc'],
			'49': game['ssc'],

			'11': game['d3'],
			'12': game['pl'],
			'37': game['kl8'],
			'102': game['ssc'],
			'103': game['ssc'],
			'104': game['ssc'],
			'112': game['klsf'],
			'113': game['klsf'],
			'114': game['klsf'],
			'123': game['klsf'],
			'109': game['k3'],
			'110': game['k3'],
			'111': game['k3'],
			'105': game['11x5'],
			'106': game['11x5'],
			'107': game['11x5'],
			'117': game['liuhecai'],
			'122': game['liuhecai'],
			'124': game['ssc'],
			'125': game['pk10'],
		};
	}

	var formatMoneyUnit = function(moneyUnit) {
		var text = "";
		switch(moneyUnit + '') {
			case '1.0000':
				text = "2元";
				break;
			case '0.5000':
				text = "1元";
				break;
			case '0.1000':
				text = "2角";
				break;
			case '0.0500':
				text = "1角";
				break;
			case '0.0100':
				text = "2分";
				break;
			case '0.0050':
				text = "1分";
				break;
			case '0.0010':
				text = "2厘";
				break;
			case '0.0005':
				text = "1厘";
				break;
			default:
				text = '';
		}
		return text;
	};

	//序列化对象
	var serializeObj2Params = function(obj) {
		var str = "";
		for(var key in obj) {
			if(str != "") {
				str += "&";
			}
			str += key + "=" + obj[key];
		}
		return str;
	}

	//取url参数
	function getQueryString(name) {
		var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
		var r = window.location.search.substr(1).match(reg);
		if(r != null) return unescape(r[2]);
		return null;
	}

	var XCOOKIE = {
		/**
		 * 设置cookie
		 * @param {string} name  键名
		 * @param {string} value 键值
		 * @param {integer} days cookie周期
		 */
		setCookie: function(name, value, days) {
			if(days) {
				var date = new Date();
				date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
				var expires = "; expires=" + date.toGMTString();
			} else {
				var expires = "";
			}
			document.cookie = name + "=" + value + expires + "; path=/";
		},
		// 获取cookie
		getCookie: function(name) {
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
			for(var i = 0; i < ca.length; i++) {
				var c = ca[i];
				while(c.charAt(0) == ' ') c = c.substring(1, c.length);
				if(c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
			}
			return null;
		},
		// 删除cookie
		deleteCookie: function(name) {
			setCookie(name, "", -1);
		}
	}

	var formatMoney = function(num, digit) {
		if(num == null) {
			return '0.00';
		}
		num = "" + num;
		if(!num) {
			return "";
		}
		num = num.replace(/,/g, '');
		if(num == '--') return num;
		var num = Number(num),
			digit = (digit == undefined || digit < 0) ? 2 : digit,
			re = /(-?\d+)(\d{3})/;
		if(Number.prototype.toFixed) {
			num = (num).toFixed(digit)
		} else {
			num = Math.round(num * Math.pow(10, digit)) / Math.pow(10, digit)
		}
		num = '' + num;
		while(re.test(num)) {
			num = num.replace(re, "$1,$2")
		}
		return num
	};

	function limitNumberInput(selector,allowPoint) {
		//限制只能输入数字
		$(selector).on('keydown', function(event) {
			if(allowPoint){
				if(event.keyCode == 190 || event.keyCode == 110){
					return true;
				}
			}
			if(!(event.keyCode == 190) && !(event.keyCode == 35) && !(event.keyCode == 36) && !(event.keyCode == 46) && !(event.keyCode == 8) && !(event.keyCode == 37) && !(event.keyCode == 39)) {
				if(!((event.keyCode >= 48 && event.keyCode <= 57) || (event.keyCode >= 96 && event.keyCode <= 105))) {
					return false;
				}

			}

		});
	}

	function emitEnterEvent(triggerArea) {
		//回车监听
		$(document).keyup(function(e) {
			var key = e.which;
			if(key == 13) {
				$(triggerArea).trigger('tap');
			}
		});
	}

	function doGetUserInfo(callback) {
		ajaxData({
			type: 'get',
			url: route.$.userInfo,
			data: {},
			successCallback: function(data) {
				callback && callback(data);
			}
		});
	}

	//充值处理
	function rechargeHandle() {
		doGetUserInfo(function(userInfo) {
			if(userInfo.is_try == 1){
				layer.alert("该账号无权限");
				return;
			}
			//没有设置资金密码
			if(userInfo.fund_pwd_seted == 0) {
				/*layer.confirm("您需要先设置资金密码", {
					btn: ['确定'],
					icon: 1,
					title: '温馨提示'
				}, function(index) {
					layer.close(index);

				});*/
				
				layer.confirm('您需要先设置资金密码', {
					btn: ['确定'],
					icon: 1,
					closeBtn: 0,
					title: '温馨提示'
				}, function(index) {
					jumpToAnotherPage('/view/personalCenter/personalCenter.shtml?personalActiveTab=fundsPasswordTab');
				});
				return;
			}

			//document.location.href = window.location.origin + '/view/personalCenter/recharge.shtml';
			var url = window.location.origin + '/view/personalCenter/recharge.shtml';
			 
			layer.open({ 
				title:"充值",
				type: 2,
				content: url,
				area: ['900px', '550px']
			});
		});
	}
	
	function getWindowHeight(){
		var height = window.screen.height - 150;
		if(height < 500){
			height == 500;
		}
		return height;
	}
	
	function jumpToAnotherPage(url){
		//如果是在iframe内页面
		if(top){
			top.document.location.href = window.location.origin + url;	
		}else{
		    document.location.href = window.location.origin + url;
		}
		
	}

	//提现处理
	function withdrawHandle() {
		doGetUserInfo(function(userInfo) {
		 
			if(userInfo.is_try == 1){
				layer.alert("该账号无权限");
				return;
			}
			if(userInfo.can_withdrawal == 0) { 
				layer.alert("该账号禁提现");
				return;
			}
			//没有设置资金密码
			if(userInfo.fund_pwd_seted == 0) {
				//layer.alert("您需要先设置资金密码");
				layer.confirm('您需要先设置资金密码', {
					btn: ['确定'],
					icon: 2,
					closeBtn: 1,
					title: '温馨提示'
				}, function(index) {
					jumpToAnotherPage('/view/personalCenter/personalCenter.shtml?personalActiveTab=fundsPasswordTab');
				});
				return;

				//没有绑卡
			} else if(userInfo.name.length == 0) {
				//layer.alert("您需要先绑定取款人");
				layer.confirm('您需要先绑定取款人', {
					btn: ['确定'],
					icon: 2,
					closeBtn: 0,
					title: '温馨提示'
				}, function(index) {
					jumpToAnotherPage('/view/personalCenter/personalCenter.shtml?personalActiveTab=bindPayeeTab');
				});
				return;
			} else if(userInfo.has_bank_card == 0) {
				//layer.alert("您需要先绑定银行卡");
				layer.confirm('您需要先绑定银行卡', {
					btn: ['确定'],
					icon: 2,
					closeBtn: 0,
					title: '温馨提示'
				}, function(index) {
					jumpToAnotherPage('/view/personalCenter/personalCenter.shtml?personalActiveTab=bankCardsTab');
				});
				return;
			} 
			
			var url = window.location.origin + '/view/personalCenter/withdraw.shtml';
			
			layer.open({
				title:"提现",
				type: 2,
				content: url,
				area: ['900px', '500px']
			});

		});
	}

	//转账处理
	function transferHandle(username, userid) {
		doGetUserInfo(function(userInfo) {
			
			if(userInfo.is_try == 1){
				layer.alert("该账号无权限");
				return;
			}
			//没有设置资金密码
			//closeBtn: 0,
			if(userInfo.fund_pwd_seted == 0) {
				//layer.alert("您需要先设置资金密码");
				
				layer.confirm('您需要先设置资金密码', {
					btn: ['确定'],
					icon: 2,
					closeBtn: 0,
					title: '温馨提示'
				}, function(index) {
					jumpToAnotherPage('/view/personalCenter/personalCenter.shtml?personalActiveTab=fundsPasswordTab');
				});
				return;

				//没有绑卡
			} else if(userInfo.name.length == 0) {
				//layer.alert("您需要先绑定取款人");
				layer.confirm('您需要先绑定取款人', {
					btn: ['确定'],
					icon: 2,
					closeBtn: 0,
					title: '温馨提示'
				}, function(index) {
					jumpToAnotherPage('/view/personalCenter/personalCenter.shtml?personalActiveTab=bindPayeeTab');
				});
				return;
			} else if(userInfo.has_bank_card == 0) {
				//layer.alert("您需要先绑定银行卡");
				layer.confirm('您需要先绑定银行卡', {
					btn: ['确定'],
					icon: 2,
					closeBtn: 0,
					title: '温馨提示'
				}, function(index) {
					jumpToAnotherPage('/view/personalCenter/personalCenter.shtml?personalActiveTab=bankCardsTab');
				});
				return;
			} 
			//jumpToAnotherPage('/view/personalCenter/transfer.shtml');
			username = username || '';
			userid = userid || '';
			localStorage.setItem('transferUserData',JSON.stringify({username:username,userid:userid}));
			var url = window.location.origin + '/view/personalCenter/transfer.shtml';
			 
			layer.open({
				title: '转账',
				shadeClose: true,
				type: 2,
				content: url,
				area: ['900px', '560px']
			});

		});

	}
	
	/**
	 *
	 * @param fn {Function}   实际要执行的函数
	 * @param delay {Number}  延迟时间，单位是毫秒（ms）
	 *
	 * @return {Function}     返回一个“防反跳”了的函数
	 */
	
	function debounce(fn, delay) {
	
	  // 定时器，用来 setTimeout
	  var timer
	
	  // 返回一个函数，这个函数会在一个时间区间结束后的 delay 毫秒时执行 fn 函数
	  return function () {
	
	    // 保存函数调用时的上下文和参数，传递给 fn
	    var context = this
	    var args = arguments
	
	    // 每次这个返回的函数被调用，就清除定时器，以保证不执行 fn
	    clearTimeout(timer)
	
	    // 当返回的函数被最后一次调用后（也就是用户停止了某个连续的操作），
	    // 再过 delay 毫秒就执行 fn
	    timer = setTimeout(function () {
	      fn.apply(context, args)
	    }, delay)
	  }
	}
	
	/**
	*
	* @param fn {Function}   实际要执行的函数
	* @param threshhold {Number}  执行间隔，单位是毫秒（ms）
	*
	* @return {Function}     返回一个“节流”函数
	*/
	
	function throttle(fn, threshhold) {
	
	  // 记录上次执行的时间
	  var last
	
	  // 定时器
	  var timer
	
	  // 默认间隔为 250ms
	  threshhold || (threshhold = 250)
	
	  // 返回的函数，每过 threshhold 毫秒就执行一次 fn 函数
	  return function () {
	
	    // 保存函数调用时的上下文和参数，传递给 fn
	    var context = this
	    var args = arguments
	
	    var now = +new Date();
	
	    // 如果距离上次执行 fn 函数的时间小于 threshhold，那么就放弃
	    // 执行 fn，并重新计时
	    if (last && now < last + threshhold) {
	      clearTimeout(timer)
	
	      // 保证在当前时间区间结束后，再执行一次 fn
	      timer = setTimeout(function () {
	        last = now
	        fn.apply(context, args)
	      }, threshhold)
	
	    // 在时间区间的最开始和到达指定间隔的时候执行一次 fn
	    } else {
	      last = now
	      fn.apply(context, args)
	    }
	  }
	}
	
	 
	function showLoadingBox(){
		$('.loading-mask').show();
	}
	
	function hideLoadingBox(){
		$('.loading-mask').hide();
	}
	
	
	var parseQueryString = function(data) {
		var array = data.split("&"),
			obj = {};
		for(var i = 0; i < array.length; i++) {
			var tmp = array[i].split('=');
			obj[tmp[0]] = tmp[1];
		}
		return obj;
	}
	
	//data  用户数据对象
	var doHandleUserFunc = function(data){
		
		if(data.is_agent){
			$('.agentCenterFLAG').show();
			$('.transferEntranceFLAG').show();
		}else{
			$('.agentCenterFLAG').hide();
			//无法进入开户页面，甚至整个代理中心都要隐藏
			$('.transferEntranceFLAG').hide();
		}
	}
	
	var doRenderUserLetterNum = function(){
		//获取未读站内信总数
		ajaxData({
			url: route.$.getUnreadNum,
			successCallback: function(msg) {
				$("#msg-num").text(msg.num);
			}
		});
	}
	
	function isLOGIN() {
		var loginFlag = localStorage.getItem('loginFlag');
		return loginFlag == 'true' ? true : false;
	}
	
	//毫秒转换成日期格式
	function timestampToTime(timestamp,type) {
        var date = new Date(timestamp * 1000);//时间戳为10位需*1000，时间戳为13位的话不需乘1000
        Y = date.getFullYear() + '-';
        M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
        D = date.getDate() < 10 ? '0'+date.getDate() : date.getDate() + ' ';
        h = (date.getHours() < 10 ? '0'+date.getHours() : date.getHours()) + ':';
        m = (date.getMinutes() < 10 ? '0'+date.getMinutes() : date.getMinutes()) + ':';
        s = date.getSeconds() < 10 ? '0'+date.getSeconds() : date.getSeconds();
        
        var time = "";
        if(type == 1){
            time = Y+M+D+h+m+s;
        }else if(type = 2){
            time = Y+M+D;
        }
        return time;
    }
	
	w.terminal_id = terminal_id;
	
	w.isLOGIN = isLOGIN;
	
	w.doRenderUserLetterNum = doRenderUserLetterNum;
	
	w.doHandleUserFunc = doHandleUserFunc;
	w.parseQueryString = parseQueryString;
	
	w.showLoadingBox = showLoadingBox;
	w.hideLoadingBox = hideLoadingBox;
	
	w.getWindowHeight = getWindowHeight;
	w.debounce = debounce;
	w.throttle = throttle;
	
	w.jumpToAnotherPage = jumpToAnotherPage;

	w.rechargeHandle = rechargeHandle;

	w.withdrawHandle = withdrawHandle;

	w.transferHandle = transferHandle;

	w.doGetUserInfo = doGetUserInfo;

	w.emitEnterEvent = emitEnterEvent;

	w.limitNumberInput = limitNumberInput;

	w.formatMoney = formatMoney;
	w.XCOOKIE = XCOOKIE;
	w.getQueryString = getQueryString;
	w.serializeObj2Params = serializeObj2Params;
	w.getWebsiteOfHost = getWebsiteOfHost;
	w.formatMoneyUnit = formatMoneyUnit;
	w.getLotteryNavObj = getLotteryNavObj;
	w.doTrender = doTrender;
	w.ajaxData = ajaxData;
	w.getLotteryUrlBySeriesId = getLotteryUrlBySeriesId;
	w.timestampToTime = timestampToTime;
})(window, jQuery)